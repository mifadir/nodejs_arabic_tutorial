var http = require('http');
var fs = require('fs');

var server = http.createServer(function(req, res){
	switch(req.method){
		case 'GET':
			switch(req.url){
				case '/stream':
					var stream = fs.createReadStream('/home/mif1/async_2.mp4');
					res.writeHead(200, {'Content-Type':'video/mp4'});
					stream.pipe(res);
					break;
				default:
					res.end("not found");
					break;
			}
			break;
		default:
			res.end();
			break;
	}
});

server.listen(8080, '127.0.0.1');
console.log("PROC["+process.pid+"] http://localhost:8080");