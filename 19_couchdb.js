var nano = require('nano')('http://localhost:5984');
var tuts = nano.db.use('tuts');

var restify = require('restify');
var server = restify.createServer();

server.get('/all/product', function(req, res, next){
	tuts.get('products_document', { revs_info: false }, function(err, body) {
	  if (!err){
	  	var msg = {
	  		status:"OK",
	  		msg:body
	  	};
	  	res.send(msg);
	  	next();
	  }else {
	  	var msg = {
	  		status:"ERR",
	  		msg:"unable to get product"
	  	};
	  	res.send(msg);
	  	next();
	  }
	});
});

server.get('/info/tuts', function(req, res, next){
	nano.db.get('tuts', function(err, body) {
	  if (!err) {
	    console.log(body);
	    var msg = {
	    	status:"OK",
	    	msg:body
	    };
	    res.send(msg);
	    next();
	  }else {
	  	var msg = {
	  		status:"ERR",
	  		msg:"unable to to get data about tuts"
	  	};
	  	res.send(msg);
	  	next();
	  }
	});
});

server.get('/all/dbs', function(req, res, next){
	nano.db.list(function(err, body) {
	  // body is an array
		var msg = {
			status:"OK",
			array_dbs:body
		};
		res.send(msg);
		next();
	});
})

server.listen(8080, '127.0.0.1');
console.log("http://localhost:8080");