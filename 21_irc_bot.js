var restify = require('restify');

var irc = require('irc');

var redis = require('redis');
var redisClient = redis.createClient();
var redisKey = 'irc:node.js:msg';

var client = new irc.Client('irc.freenode.net', 'mifadir1', {
    channels: ['#node.js'],
});

client.addListener('message#node.js', function (from, message) {
	var irc_msg = {
		date:new Date(),
		from:from,
		message:message
	};



	redisClient.lpush(redisKey, JSON.stringify(irc_msg), function(err, lpushRes){
		if(err){
			console.dir(err);
		}else {
			console.dir(irc_msg);
		}
	});
});

var server = restify.createServer();

server.get('/irc/api/josn', function(req, res, next){
	redisClient.lrange(redisKey, 0, -1, function(err, lrangeRes){
		if(err){
			var msg = {
				status:"ERR",
				msg:err
			};
			res.send(msg);
		}else {
			var list = [];
			for(var i=0;i<lrangeRes.length;i++){
				var msg_obj = JSON.parse(lrangeRes[i]);
				list.push(msg_obj);
			}
			var msg = {
				status:"OK",
				msg:list
			};
			res.send(msg);
		}
	});
});

server.post('/irc', function(req, res, next){
	// parse body
	// send msg to irc channel
});

server.listen(8080, '127.0.0.1');
console.log("PROC"+process.pid+"] http://localhost:8080");