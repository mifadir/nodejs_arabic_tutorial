var redis = require('redis'),
redisClient = redis.createClient();

var node_uuid = require('node-uuid');
var http = require('http');
var fs = require('fs');

var formidable = require('formidable');

var server = http.createServer(function(req, res){
	switch(req.method){
		case 'GET':
			switch(req.url){
				case '/':
					fs.createReadStream(__dirname+'/upload.html').pipe(res);
					break;
				case '/json/api':
					break
				default:
					res.end("not found"):
					break;
			}
			break;
		case 'POST':
			var form = new formidable.IncomingForm();
			form.uploadDir = __dirname+'/temp';
			
			form.parse(req, function(err, fields, files){
				if(err){
					var msg = {
						status:"ERR",
						msg:"unable to upload"
					};
					res.end(JSON.stringify(msg));
				}else {
					fs.exists(files.Uploaded.path, function(bool){
						if(bool){
							var fullpath = __dirname+'/data/'+files.Uploaded.name;
							fs.rename(files.Uploaded.path, fullpath, function(err){
								if(err){
									var msg = {
										status:"ERR",
										msg:"unable to rename"
									};
									res.end(JSON.stringify(msg));
								}else {

									var redisKey = 'upload:tutorial:list';

									var msg_obj = {
										uuid:node_uuid.v1(),
										date:new Date(),
										name:files.Uploaded.name,
										type:files.Uploaded.type
									};

									redisClient.lpush(redisKey, JSON.stringify(msg_obj), function(err, lpushRes){
										if(err){
											var msg = {
												status:"ERR",
												msg:"redis unable to lpush"
											};
											res.end(JSON.stringify(msg));
										}else {
											console.dir(msg_obj);

											var msg = {
												status:"OK",
												msg:"done"
											};
											res.end(JSON.stringify(msg));
										}
									});
								}
							});
						}else {
							var msg = {
								status:"ERR",
								msg:"file not found"
							};
							res.end(JSON.stringify(msg));
						}
					});
				}
			});

			break
		default:
			res.end();
			break;
	}
});

server.listen(8080, '127.0.0.1');
console.log("http://localhost:8080");