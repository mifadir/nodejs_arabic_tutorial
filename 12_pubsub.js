var joystick = new (require('joystick'))(0, 3500, 350);

var redis = require('redis');
var redisPub = redis.createClient();

joystick.on('axis', function(data){
	console.dir(data);
	redisPub.publish('my_event', JSON.stringify(data));
});