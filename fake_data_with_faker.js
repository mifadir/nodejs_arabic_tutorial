var faker = require('faker');
var node_uuid = require('node-uuid');


var mysql      = require('mysql');
var connection = mysql.createConnection({
  database : 'test',
  host     : 'localhost',
  user     : 'root',
  password : ''
});

for(var i=0;i<200;i++){
	var msg = {
		uuid:node_uuid.v1(),
		name:faker.name.firstName(),
		email:faker.internet.email(),
		phone:faker.phone.phoneNumber()
	}
	var query = 'insert into users values(?, ?, ?, ?)';
	connection.query(query, [msg.uuid, msg.name, 
		msg.email, msg.phone], function(err, res) {
  		if(err){
  			console.dir(err);
  		}else {
  			console.dir(res);
  		}
  	});
}