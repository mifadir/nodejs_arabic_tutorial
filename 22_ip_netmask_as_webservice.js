var ip = require('ip');
var qs = require('querystring');

var restify = require('restify');

var server = restify.createServer({
  name: 'ip_calc',
  version: '1.0.0'
});

server.use(restify.acceptParser(server.acceptable));
server.use(restify.queryParser());
server.use(restify.bodyParser());

server.post('/ip', function(req, res, next){
	var data = qs.parse(req.body);
	console.dir(data);
	if(data.ip!=undefined && data.mask!=undefined){
		var res_data = ip.subnet(data.ip, data.mask);
		var msg = {
			status:"OK",
			msg:res_data
		};
		res.send(msg);
	}else {
		var msg = {
			status:"ERR",
			msg:"invalid data"
		};
		res.send(msg);
	}
});

server.listen(8080, '127.0.0.1');
console.log("PROC["+process.pid+"] http://localhost:8080");