var joystick = new (require('joystick'))(0, 3500, 350);
var child_process = require('child_process').exec


joystick.on('button', function(data){
	if(data.number===1 && data.value===1){
		child_process("sensors", function(err, stdout, stderr){
			if(err){
				console.dir(err);
			}else {
				if(stderr){
					console.dir(stderr);
				}else {
					console.log(stdout);
				}
			}
		})
	}
});


joystick.on('axis', function(data){
	console.dir(data);
});
