var cluster = require('cluster');
var os = require('os');

var fullpath = '/home/mif1/nodejs4arabe/vetrine_frontside/public/img/';
var fs = require('fs');

var node_uuid = require('node-uuid');

var mysql = require('mysql');
var connection = mysql.createConnection({
	user:'root',
	password:'',
	database:'test',
	host:'localhost'
});


if(cluster.isMaster){
	for(var i=0;i<os.cpus().length;i++){
		cluster.fork();
	}
}else {
	var express = require('express');
	var app = express();

	/*
	connection.query('select 1+1 as sum, now() as time', function(err, rows, fields){
		if(err){
			console.dir(err);
		}else {
			console.dir(rows);
		}
	});
	*/

	/*
	fs.readdir(fullpath, function(err, list){
		list.forEach(function(f){
			fs.readFile(fullpath+f, function(err, blob){
				if(err){
					console.dir(err);
				}else {
					var query = 'insert into images values(?, ?)';
					connection.query(query, [node_uuid.v1(), blob], function(err, res){
						if(err){
							console.dir(err);
						}else {
							console.dir(res);
						}
					});
				}
			});
		});
	});

	*/

	app.get('/api/json', function(req, res){
		var query = 'select uuid from images';
		connection.query(query, function(err, rows, fields){
			if(err){
				var msg = {
					status:"ERR",
					msg:err
				};
				res.send(msg);
			}else {
				var msg = {
					status:"OK",
					msg:rows
				};
				res.send(msg);
			}
		});
	});


	app.get('/api/img/:uuid', function(req, res){
		var uuid = req.params.uuid;
		var query = 'select image from images where uuid=?';
		connection.query(query, [uuid], function(err, rows, fields){
			if(err){
			var msg = {
					status:"ERR",
					msg:err
				};	
			}else {
				if(rows.length==0){
					var msg = {
						status:"ERR",
						msg:"image not found"
					};
					res.send(msg);
				}else {
					res.setHeader('Content-Type', 'image/jpg');
					res.end(rows[0].image);	
				}
			}
		});

	});

	app.listen(8080, '127.0.0.1');
	console.log("PROC["+process.pid+"] http://localhost:8080");
}