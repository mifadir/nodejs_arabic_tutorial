var cloudinary = require('cloudinary');
var fs = require('fs');

var redis = require('redis');
var redisClient = redis.createClient();

var redisKey = 'cloudinary:list:json';

cloudinary.config({ 
  cloud_name: 'mifadir', 
  api_key: '', 
  api_secret: '' 
});

fs.readdir(__dirname+'/pics', function(err, list){
	if(err){
		console.dir(err);
	}else {
		list.forEach(function(pic){
			cloudinary.uploader.upload(__dirname+'/pics/'+pic, function(result) { 
  				redisClient.lpush(redisKey, JSON.stringify(result), function(err, lpushRes){
  					if(err){
  						console.dir(err);
  					}else {
  						console.dir(result);
  					}
  				});
			});
		});
	}
});