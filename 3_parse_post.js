var http = require('http');
var qs = require('querystring');

function parsePost(req, callback){

	var d = '';

	req.on('data', function(data){
		d+=data;
	});

	req.on('end', function(){
		callback(d);
	});
}

var list = [];

var server = http.createServer(function(req, res){
	switch(req.method){
		case 'GET':
			switch(req.url){
				case '/':
					res.end(" im in home\n");
					break;
				case '/home':
					res.end(" im in home 2\n");
					break;
				case '/contact':
					res.end(" im in contact \n");
					break;
				case '/api/json':
					var msg = {
						status:"OK",
						msg:list
					};
					res.end(JSON.stringify(msg));
					break;
				default:
					res.end(" not found \n");
					break;
			}
			break;
		case 'POST':
			parsePost(req, function(data){
				var parsedData = qs.parse(data);
				list.push(parsedData);
				var msg = {
					status:"OK",
					msg:"data sent"
				};
				res.end(JSON.stringify(msg));
			});
			break;
		default:
			res.end(" not implemented \n");
			break;
	}
});

server.listen(8080, '0.0.0.0');
console.log("http://0.0.0.0:8080");