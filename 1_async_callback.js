var fs = require('fs');

function Add(a, b, callback){
	callback(a+b);
}

setTimeout(function(){
	console.log("Hello");
}, 3000);

Add(1, 2, function(data1){
	Add(3, data1, function(data2){
		console.log(data2);
	});
});

fs.stat('/etc/hosts', function(err, data){
	if(err){
		console.dir(err);
	}else {
		console.dir(data);
	}
});