var net = require('net');
var node_uuid = require('node-uuid');

var os = require('os');

var client_list = [];

var e = 'distributed_event_';

var redis = require('redis');
var redisPub = redis.createClient(6379, '192.168.2.4');
var redisSub = redis.createClient(6379, '192.168.2.4');

var hostname = os.hostname();
var remote = 'mif-pigeon';

var server = net.createServer(function(socket){
	socket.uuid = node_uuid.v1();

	socket.on('data', function(data){
		console.log("client with uuid:"+socket.uuid+" sent data");
		for(var i=0;i<client_list.length;i++){
			if(client_list[i].uuid!=socket.uuid){
				client_list[i].write(data);
			}
		}

		redisPub.publish(e+remote, data);
	});

	client_list.push(socket);


	socket.on('end', function(){
		console.log("client with uuid:"+socket.uuid+" is gone");
		for(var i=0;i<client_list.length;i++){
			if(client_list[i].uuid===socket.uuid){
				client_list.splice(i, 1);
			}
		}
	});

});

redisSub.subscribe(e+hostname);

redisSub.on('message', function(channel, message){
	for(var i=0;i<client_list.length;i++){
		client_list[i].write(message);
	}
});

server.listen(8080, '0.0.0.0');
console.log("tcp://127.0.0.1:8080");