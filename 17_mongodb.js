var restify = require('restify');
var qs = require('querystring');
var mongodb = require('mongodb').MongoClient;

// var app = http.createServer(function(req, res){ })

var server = restify.createServer({
  name: 'mongodb_tutorial',
  version: '0.0.1'
});

var url = 'mongodb://127.0.0.1:27017/tutorial_test_db';

server.use(restify.acceptParser(server.acceptable));
server.use(restify.queryParser());
server.use(restify.bodyParser());

mongodb.connect(url, function(err, db){
	if(err){
		console.dir(err);
	}else {

		var tutorial = db.collection("tutorial");

		server.get('/tutorial', function(req, res, next){
			tutorial.find().toArray(function(err, results) {
	       		if(err){
	       			var msg = {
	       				status:"ERR",
	       				msg:err
	       			};
	       			res.send(msg);
	       			next();
	       		}else {
	       			var msg = {
	       				status:"OK",
	       				msg:results
	       			};
	       			res.send(msg);
	       			next();
	       		}
	      	});
		});

		server.post('/tutorial', function(req, res, next){
			var data = qs.parse(req.body);
			var msg = {
				name:data.name,
				duration:data.duration,
				description:data.description
			};
			console.dir(msg);
			tutorial.insert(msg, function(err, resu){
				if(err){
					var msg = {
						status:"ERR",
						msg:err
					};
					res.send(msg);
					next();
				}else {
					var msg = {
						status:"OK",
						msg:resu
					};
					res.send(msg);
					next();
				}
			});
		});

		server.put('/tutorial', function(req, res, next){

		});

		server.del('/tutorial', function(req, res, next){

		});

		server.listen(8080, '127.0.0.1');
		console.log("http://localhost:8080");
	}
});