var http = require('http');
var url = "http://api.openweathermap.org/data/2.5/weather?q=Marrakesh,ma";

function getMarrakeshTemp(){
	http.get(url, function(response){

		var d = '';

		response.on('data', function(data){
			d+=data;
		});

		response.on('end', function(){
			var ParsedData = JSON.parse(d);
			var msg = {
				date:new Date(),
				temp:ParsedData.main.temp-273.15+" °C"
			};
			console.log(msg);		
		});
	});
}

setInterval(function(){
	getMarrakeshTemp();
}, 3000);