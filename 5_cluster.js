var cluster = require('cluster');
var os = require('os');

if(cluster.isMaster){
	// master logic

	console.log("MASTER PID:"+process.pid);

	for(var i=0;i<os.cpus().length;i++){
		cluster.fork();
	}

	
	cluster.on('fork', function(){
		console.log(" oui :)");
	});

	cluster.on('exit', function(worker){
		console.log(" le processus avec pid:"+worker.process.pid+" est mort");
		setTimeout(function(){
			cluster.fork();
		}, 3000);
	});

}else {
	// worker logic
	var app = require(__dirname+'/7_tcp_server.js');
}