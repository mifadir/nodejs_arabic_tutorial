var net = require('net');
var node_uuid = require('node-uuid');

var client_list = [];

var server = net.createServer(function(socket){
	socket.uuid = node_uuid.v1();

	socket.on('data', function(data){
		console.log("client with uuid:"+socket.uuid+" sent data");
		for(var i=0;i<client_list.length;i++){
			if(client_list[i].uuid!=socket.uuid){
				client_list[i].write(data);
			}
		}
	});

	client_list.push(socket);


	socket.on('end', function(){
		console.log("client with uuid:"+socket.uuid+" is gone");
		for(var i=0;i<client_list.length;i++){
			if(client_list[i].uuid===socket.uuid){
				client_list.splice(i, 1);
			}
		}
	});
});

server.listen(8080, '127.0.0.1');
console.log("tcp://127.0.0.1:8080");