var http = require('http');

var server = http.createServer(function(req, res){
	switch(req.method){
		case 'GET':
			switch(req.url){
				case '/':
					console.log(process.pid);
					res.end(" im in home my pid is:"+process.pid+"\n");
					break;
				case '/home':
					res.end(" im in home 2\n");
					break;
				case '/contact':
					res.end(" im in contact \n");
					break;
				default:
					res.end(" not found \n");
					break;
			}
			break;
		case 'POST':
			res.end(" post is ok \n");
			break;
		default:
			res.end(" not implemented \n");
			break;
	}
});

server.listen(8080, '0.0.0.0');
console.log("PROC["+process.pid+"] http://0.0.0.0:8080");