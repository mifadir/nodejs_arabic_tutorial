var node_uuid = require('node-uuid');
var faker = require('faker');

var mqtt = require('mqtt');
var mq = mqtt.connect('mqtt://localhost');

setInterval(function(){
	var msg = {
		uuid:node_uuid.v1(),
		sentense:faker.lorem.sentence()
	};

	mq.publish('test_event', JSON.stringify(msg));
}, 1000);

