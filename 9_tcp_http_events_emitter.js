var http = require('http');
var net = require('net');

var joystick = new (require('joystick'))(0, 3500, 350);
var events = require('events');

var e = new events.EventEmitter();

joystick.on('axis', function(data){
	e.emit('joystick_event', JSON.stringify(data));
});


var http_server = http.createServer(function(req, res){
	e.on('joystick_event', function(data){
		res.write(data);
	});
});

var tcp_server = net.createServer(function(socket){
	e.on('joystick_event', function(data){
		socket.write(data);
	});
});

http_server.listen(8080, '127.0.0.1');
tcp_server.listen(8081, '127.0.0.1');

console.log("PROC["+process.pid+"] http://localhost:8080");
console.log("PROC["+process.pid+"] tcp://localhost:8081");





