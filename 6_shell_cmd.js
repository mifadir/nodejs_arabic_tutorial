var childprocess = require('child_process').exec;
var qs = require('querystring');
var http = require('http');

function parsePost(req, callback){

	var d = '';

	req.on('data', function(data){
		d+=data;
	});

	req.on('end', function(){
		callback(d);
	});
}

var server = http.createServer(function(req, res){
	
	switch(req.method){
		case 'POST':
			parsePost(req, function(data){
				//cmd=ls
				var parsedData = qs.parse(data);
				childprocess(parsedData.cmd, function(err, stdout, stderr){
					if(err){
						res.end(err);
					}else {
						if(stderr){
							res.end(stderr);
						}else {
							res.end(stdout);
						}
					}
				});
			});
			break;
		default:
			res.end();
	}
});

server.listen(8080, '127.0.0.1');
console.log("PROC["+process.pid+"] http://localhost:8080");